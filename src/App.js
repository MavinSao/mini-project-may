import './App.css';
import Menu from './components/Menu';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Article from './components/Article';
import Category from './components/Category';
import React, { Component } from 'react'
import strings from './localization/string'
import NewArticle from './components/NewArticle';
import View from './components/View';
export default class App extends Component {

  onChangeLanguage = (str) => {
    strings.setLanguage(str);
    this.setState({});
  }

  render() {
    return (
      <div>
        <Router>
          <Menu onChangeLanguage={this.onChangeLanguage} />
          <Switch>
            <Route path='/' exact component={Article} />
            <Route path='/Category' component={Category} />
            <Route path='/new-article' component={NewArticle} />
            <Route path='/View/:id' component={View} />
            <Route path='*' render={() => <div className="container my-5"><h1>404 Page Not Found</h1></div>} />
          </Switch>
        </Router>
      </div>
    )
  }
}

