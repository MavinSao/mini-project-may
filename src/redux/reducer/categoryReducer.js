import { categoryAction as categoryType } from './../actions/actionType'

const initState = {
    category: [],
}
export const categoryReducer = (state = initState, action) => {
    switch (action.type) {
        case categoryType.fetchCategory: {
            return { category: action.payLoad }
        }
        case categoryType.deleteCategory:
            return { category: state.category.filter((c) => c._id !== action.payLoad) }
        default:
            return state
    }
}