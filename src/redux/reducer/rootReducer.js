import { combineReducers } from 'redux';
import articleReducer from './articleReducer'
import { categoryReducer } from './categoryReducer';
const rootReducer = combineReducers({
    articleR: articleReducer,
    categoryR: categoryReducer
});
export default rootReducer;
