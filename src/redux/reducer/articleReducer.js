import { articleAction as actionType } from './../actions/actionType'

const initState = {
    articles: [],
    filterArticle: []
}
const articleReducer = (state = initState, action) => {
    switch (action.type) {
        case actionType.fetchArticle:
            return { ...state, articles: [...action.payLoad] }
        case actionType.fetchMoreArticle:
            return { ...state, articles: [...state.articles, ...action.payLoad] }
        case actionType.deleteArticle:
            return { ...state, articles: state.articles.filter((a) => a._id !== action.payLoad) }
        case actionType.searchArticle:
            return { ...state, articles: action.payLoad }
        case actionType.filterByCategory: {
            let filterData = state.articles.filter((a) => (a.category ? a.category._id : null) === action.payLoad)
            return { ...state, filterArticle: filterData }
        }
        default:
            return state
    }
}
export default articleReducer