export const articleAction = {
    fetchArticle: "fetch_article",
    fetchMoreArticle: "fetch_more_article",
    deleteArticle: "delete_article",
    filterByCategory: "filter_by_category",
    searchArticle: "search_article",
}
export const categoryAction = {
    fetchCategory: "fetch_catefory",
    deleteCategory: "delete_catefory",
    updateCategory: "update_catefory",
}
