import { categoryAction } from './actionType'
import axios from 'axios'
import { base_url } from '../../url/url'

export const fetchCategory = () => {
    return (dp) => {
        axios.get(`${base_url}/category`).then(res => {
            dp({
                type: categoryAction.fetchCategory,
                payLoad: res.data.data
            })
        })
    }
}
export const postCategory = (category, cb) => {
    axios.post(`${base_url}/category`, category).then((res) => {
        cb(res)
    })
}
export const deleteCategory = (id) => {
    return (dp) => {
        axios.delete(`${base_url}/category/${id}`).then(res => {
            dp({
                type: categoryAction.deleteCategory,
                payLoad: id
            })
        })
    }
}
export const updateCategory = (id, category, cb) => {
    axios.put(`${base_url}/category/${id}`, category).then(res => {
        cb(res)
    })
}