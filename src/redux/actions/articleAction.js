import { articleAction } from './actionType'
import axios from 'axios'
import { base_url } from '../../url/url'

export const getArticles = () => {
    return (dp) => {
        axios.get(`${base_url}/articles?page=1&size=10`).then(res => {
            dp({
                type: articleAction.fetchArticle,
                payLoad: res.data.data
            })
        })
    }
}

export const getMoreArticles = (page, callBack) => {
    return (dp) => {
        axios.get(`${base_url}/articles?page=${page}&size=10`).then(res => {
            dp({
                type: articleAction.fetchMoreArticle,
                payLoad: res.data.data
            })
            callBack(res.data.data.length)
        })
    }
}

export const deleteArticle = (id) => {
    return (dp) => {
        axios.delete(`${base_url}/articles/${id}`).then(res => {
            dp({
                type: articleAction.deleteArticle,
                payLoad: id
            })
        })
    }
}
export const searchArticle = (title) => {
    return (dp) => {
        axios.get(`${base_url}/articles?title=${title}`).then(res => {
            dp({
                type: articleAction.searchArticle,
                payLoad: res.data.data
            })
        })
    }
}

export const addArticle = (article, callBack) => {
    axios.post(`${base_url}/articles`, article).then(res => {
        callBack(res.data.message)
    })
}
export const updateArticle = (article, uId, callBack) => {
    axios.patch(`${base_url}/articles/${uId}`, article).then(res => {
        callBack(res.data.message)
    })
}

export const fetchOne = (id, callBack) => {
    axios.get(`${base_url}/articles/${id}`).then(res => {
        callBack(res.data.data)
    })
}

export const uploadImage = (file, callBack) => {
    axios.post(`${base_url}/images`, file).then(res => {
        callBack(res.data.url)
    })
}

export const filterByCategory = (catId) => {
    return {
        type: articleAction.filterByCategory,
        payLoad: catId
    }
}

