import { Form, Button } from "react-bootstrap";
import React, { Component } from "react";
import strings from "./../localization/string";
import { fetchCategory } from "./../redux/actions/categoryAction";
import {
  uploadImage,
  addArticle,
  updateArticle,
  fetchOne,
} from "./../redux/actions/articleAction";
import queryString from "query-string";
import { connect } from "react-redux";
import Radio from "./radioButton/Radio";

class NewArticle extends Component {
  constructor() {
    super();
    this.state = {
      isUpdate: false,
      imgFile: "",
      imgLink: "",
      updateId: "",
      title: "",
      categoryId: 0,
      description: "",
      titleError: "",
      descriptionError: "",
    };
    this.handleChangeImage = this.handleChangeImage.bind(this);
  }

  componentWillMount() {
    this.props.fetchCategory();
    let path_data = queryString.parse(this.props.location.search);
    if (path_data.id !== undefined) {
      fetchOne(path_data.id, (data) => {
        this.setState({
          updateId: path_data.id,
          title: data.title,
          description: data.description,
          categoryId: data.category ? data.category._id : null,
          isUpdate: Boolean(path_data.update),
          imgLink: data.image,
        });
      });
    }
  }

  handleChangeImage = (e) => {
    this.setState({
      imgFile: e.target.files[0],
      imgLink: URL.createObjectURL(e.target.files[0]),
    });
  };

  validate = () => {
    let titleError = "";
    let descriptionError = "";
    if (!this.state.title) {
      titleError = "* Title can not be blank";
    }
    if (!this.state.description) {
      descriptionError = "* Description can not be blank";
    }
    if (titleError || descriptionError) {
      this.setState({
        titleError,
        descriptionError,
      });
      return false;
    }
    return true;
  };

  newArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImage(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        addArticle(article, (msg) => {
          alert(msg);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
      };
      addArticle(article, (msg) => {
        alert(msg);
        this.props.history.push("/");
      });
    }
  };

  updateArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      uploadImage(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        updateArticle(article, this.state.updateId, (message) => {
          alert(message);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
        image: this.state.imgLink,
      };
      updateArticle(article, this.state.updateId, (message) => {
        alert(message);
        this.props.history.push("/");
      });
    }
  };

  handleSubmit = () => {
    let isValid = this.validate();
    if (isValid) {
      this.state.isUpdate ? this.updateArticle() : this.newArticle();
    }
  };

  handleRadio = (e) => {
    this.setState({
      categoryId: e.target.value,
    });
  };

  handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value,
    });
  };

  render() {
    let categories = this.props.category.map((cat) => (
      <Radio
        key={cat._id}
        category={cat}
        handleRadio={this.handleRadio}
        categoryId={this.state.categoryId}
      />
    ));

    return (
      <div className="container">
        <h1 className="my-4">
          {this.state.isUpdate ? strings.update : strings.add}
        </h1>
        <div className="row">
          <div className="col-md-7">
            <Form>
              <Form.Group controlId="title">
                <Form.Label>{strings.title}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input title"
                  value={this.state.title}
                  name="title"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="text-danger">
                  {this.state.titleError}
                </Form.Text>
              </Form.Group>
              <Form.Group>
                <Form.Label className="mr-3">{strings.category} :</Form.Label>
                {categories}
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>{strings.description}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Description"
                  value={this.state.description}
                  name="description"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="text-danger">
                  {this.state.descriptionError}
                </Form.Text>
              </Form.Group>
              <Form.Label>{strings.thumbnail}</Form.Label>
              <Form.File
                id="custom-file-translate-scss"
                label="Custom file input"
                lang="en"
                onChange={this.handleChangeImage}
                custom
              />
              <Button
                variant="primary"
                className="my-3"
                onClick={() => this.handleSubmit()}
              >
                {this.state.isUpdate ? "Save" : "Submit"}
              </Button>
            </Form>
          </div>
          <div className="col-md-5">
            <img
              src={
                this.state.imgLink === "" || this.state.imgLink === undefined
                  ? "https://www.slingshotvoip.com/wp-content/uploads/2019/12/placeholder-300x200.png"
                  : this.state.imgLink
              }
              alt="img"
              className="img-fluid"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mtp = (store) => {
  return {
    category: store.categoryR.category,
  };
};

export default connect(mtp, { addArticle, fetchCategory })(NewArticle);
