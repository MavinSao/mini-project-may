import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import strings from "./../localization/string";

export const formatDate = (dateTime) => {
  const date = new Date(dateTime);
  const yy = date.getFullYear();
  const mm = date.getMonth();
  const dd = date.getDate();
  const Dates = [yy, mm, dd];
  return Dates.join("-");
};

function Item({ article, deleteArticle }) {
  return (
    <div>
      <div className="row">
        <div className="col-md-4">
          <img
            className="img-fluid rounded mb-3 mb-md-0"
            src={
              article.image
                ? article.image
                : "https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png"
            }
            alt="thumnail"
          />
        </div>
        <div className="col-md-8">
          <h3>{article.title}</h3>
          <p className="text-muted mb-0">
            {strings.createDate} : {formatDate(article.createdAt)}
          </p>
          <p className="text-muted mt-0">
            {strings.category} :{" "}
            {article.category ? article.category.name : "No Type"}
          </p>
          <p>{article.description}</p>
          <br />
          <Link to={`/View/${article._id}`}>
            <Button variant="primary" size="md">
              {strings.view}
            </Button>
          </Link>
          <Link to={`/new-article?update=true&&id=${article._id}`}>
            <Button variant="warning" className="mx-1" size="md">
              {strings.edit}
            </Button>
          </Link>
          <Button
            variant="danger"
            size="md"
            onClick={() => deleteArticle(article._id)}
          >
            {strings.delete}
          </Button>
        </div>
      </div>
      <hr />
    </div>
  );
}

export default Item;
