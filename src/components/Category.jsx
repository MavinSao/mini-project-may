import { Table, Button, Form } from "react-bootstrap";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchCategory,
  postCategory,
  deleteCategory,
  updateCategory,
} from "./../redux/actions/categoryAction";
import strings from "../localization/string";

class Category extends Component {
  constructor() {
    super();
    this.state = {
      nameError: "",
      name: "",
      isUpdate: false,
      updateId: "",
    };
  }
  componentWillMount() {
    this.props.fetchCategory();
  }
  handleChange = (e) => {
    this.setState({
      name: e.target.value,
    });
  };

  validate = () => {
    let nameError = "";
    if (!this.state.name) {
      nameError = "* Category name can not be blank";
    }
    if (nameError) {
      this.setState({
        nameError,
      });
      return false;
    }
    return true;
  };

  handleAddUpdate = () => {
    if (this.validate()) {
      const category = {
        name: this.state.name,
      };
      if (this.state.isUpdate) {
        updateCategory(this.state.updateId, category, (res) => {
          this.props.fetchCategory();
          alert(res.data.message);
          this.setState({
            isUpdate: false,
            name: "",
            nameError: "",
          });
        });
      } else {
        postCategory(category, (res) => {
          this.props.fetchCategory();
          alert(res.data.message);
          this.setState({
            name: "",
            nameError: "",
          });
        });
      }
    }
  };

  handleEdit = (id, name) => {
    this.setState({
      name: name,
      updateId: id,
      isUpdate: true,
    });
  };

  render() {
    let categories = this.props.category.map((cat, index) => (
      <tr key={cat._id}>
        <td>{index + 1}</td>
        <td>{cat.name}</td>
        <td>
          <Button
            variant="primary"
            className="mx-1"
            size="md"
            onClick={() => this.handleEdit(cat._id, cat.name)}
          >
            {strings.edit}
          </Button>
          <Button
            variant="danger"
            className="mx-1"
            size="md"
            onClick={() => this.props.deleteCategory(cat._id)}
          >
            {strings.delete}
          </Button>
        </td>
      </tr>
    ));
    return (
      <div className="container">
        <h1 className="my-4">{strings.category}</h1>
        <Form.Group controlId="Name">
          <Form.Label>{strings.name}</Form.Label>
          <div className="form-inline w-75">
            <Form.Control
              type="text"
              placeholder="Input Category Name"
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
            />

            <Button
              variant="dark"
              className="mx-1"
              size="md"
              onClick={this.handleAddUpdate}
            >
              {this.state.isUpdate ? "Save" : strings.addCartegory}
            </Button>
          </div>
          <Form.Text className="text-danger">{this.state.nameError}</Form.Text>
        </Form.Group>

        <Table striped bordered hover className="text-center">
          <thead>
            <tr>
              <th>#</th>
              <th>{strings.name}</th>
              <th>{strings.action}</th>
            </tr>
          </thead>
          <tbody>{categories}</tbody>
        </Table>
      </div>
    );
  }
}
const mapStateToProps = (store) => {
  return {
    category: store.categoryR.category,
  };
};
export default connect(mapStateToProps, { fetchCategory, deleteCategory })(
  Category
);
