import { Button, Form, FormControl } from "react-bootstrap";
import { connect } from "react-redux";
import React, { PureComponent } from "react";
import Item from "./Item";
import {
  searchArticle,
  getArticles,
  getMoreArticles,
  deleteArticle,
  filterByCategory,
} from "./../redux/actions/articleAction";
import { fetchCategory } from "./../redux/actions/categoryAction";
import strings from "./../localization/string";
import { Link } from "react-router-dom";
import ScrollData from "./ScollData";

class Article extends PureComponent {
  constructor() {
    super();
    this.state = {
      page: 1,
      hasMore: true,
      filterPage: 1,
    };
  }

  getNextArticles = (page) => {
    this.props.getMoreArticles(page, (lengthData) => {
      console.log(lengthData);
      if (lengthData === 0) {
        this.setState({
          hasMore: false,
        });
      }
    });
  };

  componentWillMount() {
    this.props.getArticles();
    this.props.fetchCategory();
  }

  fetchMoreData = () => {
    this.setState(
      (pre) => {
        return { page: pre.page + 1 };
      },
      () => this.getNextArticles(this.state.page)
    );
  };

  handleOption = (e) => {
    this.props.filterByCategory(e.target.value);
  };

  render() {
    var articles = [];
    if (this.props.filterArticle.length === 0) {
      articles = this.props.articles.map((article) => (
        <Item
          article={article}
          key={article._id}
          deleteArticle={this.props.deleteArticle}
        />
      ));
    } else {
      articles = this.props.filterArticle.map((article) => (
        <Item
          article={article}
          key={article._id}
          deleteArticle={this.props.deleteArticle}
        />
      ));
    }

    return (
      <div className="container">
        <div className="row my-4">
          <div className="col-lg-5 col-md-12 col-sm-12 ">
            <h1>{strings.articleM}</h1>
          </div>
          <div className="col-lg-7 col-md-12 col-sm-12 text-right ">
            <Form inline className="justify-content-end">
              <div className="row">
                <Form.Group>
                  <Form.Label>Category</Form.Label>
                  <Form.Control
                    as="select"
                    onChange={this.handleOption}
                    className="mx-2"
                    custom
                  >
                    <option value={null}>All</option>
                    {this.props.category.map((cat) => (
                      <option key={cat._id} value={cat._id}>
                        {cat.name}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>

                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                  onChange={(e) => this.props.searchArticle(e.target.value)}
                />
                <Button variant="outline-success">{strings.search}</Button>
                <Link to="/new-article">
                  <Button variant="primary" className="mx-2" size="md">
                    {strings.add}
                  </Button>
                </Link>
              </div>
            </Form>
          </div>
        </div>
        {this.props.filterArticle.length === 0 ? (
          <ScrollData
            articles={articles}
            fetchMoreData={this.fetchMoreData}
            hasMore={this.state.hasMore}
          />
        ) : (
          articles
        )}
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    filterArticle: store.articleR.filterArticle,
    articles: store.articleR.articles,
    category: store.categoryR.category,
  };
};

const mapDispatchToProps = {
  searchArticle,
  getArticles,
  getMoreArticles,
  fetchCategory,
  deleteArticle,
  filterByCategory,
};

export default connect(mapStateToProps, mapDispatchToProps)(Article);
