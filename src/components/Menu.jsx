import React from "react";
import strings from "./../localization/string";
import { Nav, Navbar, NavDropdown, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import logo from "./../img/logo.png";
function Menu({ onChangeLanguage }) {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">
          <img
            src={logo}
            alt="logo"
            className="mr-2"
            style={{ width: "40px" }}
          />
          {strings.articleM}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">
              {strings.article}
            </Nav.Link>
            <Nav.Link as={Link} to="/Category">
              {strings.category}
            </Nav.Link>
            <NavDropdown title={strings.lang} id="basic-nav-dropdown">
              <NavDropdown.Item onClick={() => onChangeLanguage("kh")}>
                {strings.kh}
              </NavDropdown.Item>
              <NavDropdown.Item onClick={() => onChangeLanguage("en")}>
                {strings.en}
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Menu;
