import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { BoxLoading } from "react-loadingg";
function ScollData({ articles, fetchMoreData, hasMore }) {
  return (
    <InfiniteScroll
      dataLength={articles.length} //This is important field to render the next data
      next={fetchMoreData}
      hasMore={hasMore}
      loader={<BoxLoading />}
      endMessage={
        <p style={{ textAlign: "center" }}>
          <b>Yay! You have seen it all 👍🏌️‍♂️🥃</b>
        </p>
      }
    >
      {articles}
    </InfiniteScroll>
  );
}

export default ScollData;
