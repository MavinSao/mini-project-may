import { fetchOne } from "../redux/actions/articleAction";
import React, { Component } from "react";
import { formatDate } from "./Item";
import strings from "../localization/string";
export default class View extends Component {
  state = {
    title: "",
    description: "",
    createDate: "",
    image: "",
  };
  componentWillMount() {
    fetchOne(this.props.match.params.id, (data) => {
      this.setState(
        {
          title: data.title,
          description: data.description,
          image: data.image,
          category: data.category,
          createDate: data.createdAt,
        },
        () => console.log(this.state)
      );
    });
  }
  render() {
    return (
      <div className="container">
        <div className="row my-5">
          <div className="col-md-4">
            <img
              className="img-fluid rounded mb-3 mb-md-0"
              src={
                this.state.image
                  ? this.state.image
                  : "https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png"
              }
              alt="thumnail"
            />
          </div>
          <div className="col-md-8">
            <h3>{this.state.title}</h3>
            <p className="text-muted">
              {strings.createDate} : {formatDate(this.state.createDate)}
            </p>
            <p className="text-muted">
              {strings.category} :{" "}
              {this.state.category ? this.state.category.name : "No Type"}
            </p>
            <p>{this.state.description}</p>
            <br />
          </div>
        </div>
      </div>
    );
  }
}
