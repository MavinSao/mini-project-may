import React from "react";

function Radio({ category, handleRadio, categoryId }) {
  return (
    <div className="custom-control custom-radio d-inline mx-2">
      <input
        type="radio"
        id={category._id}
        name="category"
        className="custom-control-input"
        value={category._id}
        onChange={handleRadio}
        checked={categoryId === category._id}
      />
      <label className="custom-control-label" htmlFor={category._id}>
        {category.name}
      </label>
    </div>
  );
}

export default Radio;
