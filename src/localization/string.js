import LocalizedStrings from 'localized-strings'
const strings = new LocalizedStrings({
    en: {
        articleM: "Article Management",
        article: "Article",
        category: "Category",
        lang: "Language",
        kh: "Khmer",
        en: "English",
        view: "View",
        edit: "Edit",
        delete: "Delete",
        search: "Search",
        add: "Add Article",
        update: "Update Article",
        createDate: "Create Date",
        title: "Title",
        description: "Description",
        thumbnail: "Thumbnail",
        save: "Save",
        name: "Name",
        action: "Action",
        addCartegory: "Add Category"
    },
    kh: {
        articleM: "ប្រព័ន្ធគ្រប់គ្រងពត័មាន",
        article: "អត្ថបទ",
        category: "ប្រភេទ",
        lang: "ភាសារ",
        kh: "ខ្មែរ",
        en: "អង់គ្លេស",
        view: "មើល",
        edit: "កែប្រែ",
        delete: "លុប",
        search: "ស្វែងរក",
        add: "បន្ថែម​ អត្ថបទ",
        update: "កែប្រែ អត្ថបទ",
        createDate: "កាលបរិច្ឆេទ",
        title: "ចំណងជើង",
        description: "ការពិពណ៌នា",
        thumbnail: "រូបតំណាង",
        save: "រក្សារទុក",
        name: "ឈ្មោះ",
        action: "សកម្មភាព",
        addCartegory: "បង្កើតប្រភេទ"
    }
})
export default strings